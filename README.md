Script to create a Server for the BikeHikeLayer using Docker
Prerequisite: install docker and git, then run: git clone https://gitlab.com/Apfelkuacha/BikeHikeLayerServer.git
afterwards you could use start.sh to get everything running

If it is not working: execute the commands in the start.sh on your own inside the docker.
Mostly the renderd tells you whats wrong. Check if it is running with: ps aux
Or start it yourself to see the logs: sudo -u $DBPG_USERNAME renderd -f
