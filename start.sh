#!/bin/bash
# Author: Lukas :)     -> there would be an existing project which uses Docker for serving tiles, but with the selfmade one you learn much more: https://github.com/Overv/openstreetmap-tile-server
# Prerequisite: install docker and git, then: git clone https://gitlab.com/Apfelkuacha/BikeHikeLayerServer.git 

RED='\033[1;31m'
GREEN='\033[1;32m'
RESETCOLOR='\033[0m' # No Color

# Settings
set -e # Be sure we fail on error and output debugging information
trap 'echo "$0: error on line $LINENO"' ERR
#set -x # Print commands and their arguments as they are executed

function ReadConfig {
    # Config reading, in there all important stuff is defined
    here="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
    . $here/.env
}

function Execute {
    commandExample='sudo docker exec [OPTIONS] /bin/bash -c COMMAND'  # that was the old one, without using docker compose
    commandExample='sudo docker exec --workdir=$DOCKERPATH_SOURCE_DIR $DOCKER_NAME /bin/bash -c "echo test1"'  # example command
    
    commandExample='sudo docker compose exec [OPTIONS] SERVICE COMMAND [ARGS...]'
    commandExample='sudo docker compose exec BikeHikeLayer /bin/bash -c "echo test2"'  # example command
    commandExample='sudo docker compose exec -it BikeHikeLayer /bin/bash'  # example to start commandline
    # TODO: if this is called inside docker -> do not call docker compose
    
    command='sudo docker compose exec'
    
    if [ ! -z "$3" ]; then command="$command --user=$3"; fi
    
    if [ -z "$2" ]; then command="$command --workdir=$DOCKERPATH_SOURCE_DIR"
    elif [ "$2" = "StyleDirectory" ]; then command="$command --workdir=$DOCKERPATH_WORKING_DIR/$LAYER_NAME/"
    elif [ "$2" = "ExecuteAsFile" ]; then :  # do nothing
    else command="$command --workdir=$2"; fi
    
    if [ -z "$1" ]; then command="$command -it"; fi  # on empty parameter append -it to command to start Bash
        
    if [ "$2" = "ExecuteAsFile" ]; then command="$command BikeHikeLayer \"$1\""
    else 
        command="$command BikeHikeLayer /bin/bash"
        if [ ! -z "$1" ]; then command="$command -c \"$1\""; fi
    fi
    
    echo "$command"
    eval "$command" # the command after eval has to be in quotes
}

function StartDocker {
    sudo docker compose up --no-recreate -d
    # with "start" or with "--no-recreate up" it starts the containers, if you use "up" it will create a new one if the configuration has changed
    # so the one doesn't take the configuration changes, the other creates a new container and the settings have to be loaded again.
    
#    InitialiseDocker
    #necessary to read changed config??    sudo docker exec --workdir=$DOCKERPATH_SOURCE_DIR $DOCKER_NAME /bin/bash -c ". ./conf/config"
}

function OpenDockerBashRoot {
    StartDocker;
    #sudo docker exec -it --workdir=$DOCKERPATH_SOURCE_DIR $DOCKER_NAME /bin/bash
    Execute
}
function OpenDockerBash {
    StartDocker;
    Execute "" "" "$DBPG_USERNAME"
}

function ImportDatabase {
    CheckFiles;

    StartDocker;
    #echo Setting OSM2PGSQL Options
    OSM2PGSQL_CMD="osm2pgsql $OSM2PGSQL_OPTS $DOCKERPATH_OSM_FILE_DIR/$OSM_LATEST_FILE_NAME"
    echo -e "That clears the Database and makes a fresh import. This could take very long, depending on country size. Check again the options: \n${GREEN}$OSM2PGSQL_CMD${RESETCOLOR}\n"
    read -p "Are you sure? [y/n]" yn
    case $yn in
        [Yy]* ) ;;
        [Nn]* ) return;;
        * ) echo "You answered no.."; return;;
    esac
    #Command to read from database: psql -c "SELECT * FROM planet_osm_line where trail_visibility IS NOT NULL;" -d databasename -U username
    #sudo docker exec --workdir=$DOCKERPATH_WORKING_DIR/$LAYER_NAME/ $DOCKER_NAME /bin/bash -c "nohup $OSM2PGSQL_CMD >> $DOCKERPATH_WORKING_DIR/osm2pgsql.log 2>&1"    # The command to run it in background with Logging
    Execute "$OSM2PGSQL_CMD"
    echo -e "\nUpdate isolations value for the mountain peaks.    Note: some warnings and errors here are fine, as long as there are loads of \"UPDATE 1\" listed\n"
    Execute "$DOCKERPATH_WORKING_DIR/$LAYER_NAME/scripts/peak_isolation/update_isolations.sh" "ExecuteAsFile"
}

function GenerateMapStyle {
    StartDocker;
    echo -e "${GREEN}Generate style from project.mml to osm.xml${RESETCOLOR}"
    #sudo docker exec --workdir=$DOCKERPATH_WORKING_DIR/$LAYER_NAME/ $DOCKER_NAME /bin/bash -c "carto project.mml >osm.xml"
    Execute "carto project.mml >osm.xml" "StyleDirectory"
    
    # todo: this check should run inside docker?! but it isn't that important any more..
    #echo Check if cache is empty
	#if [ "$(sudo ls -A /var/cache/renderd/tiles)" ]; then
    #    read -p "The renderd cache is not empty, do you want to delete it to newly generate all tiles? [y/*]" yn
    #    case $yn in
    #        [Yy]* )
    #            sudo rm -rf /var/cache/renderd/tiles/*
    #            RestartRenderd;  # after clearing cache renderd should be restarted
    #            ;;
    #        * ) ;;
    #    esac
    #fi
}
function RestartRenderd {
    #Execute "pkill renderd" || true    #sudo docker exec $DOCKER_NAME /bin/bash -c "pkill renderd" || true
    #Execute "renderd" "" "renderd" # start renderd as user "renderd"
    Execute "service renderd restart" "" "_renderd" # hint: "service renderd status" tells that it failed. But with "sudo -u _renderd service renderd status" you could see that it's running
}
function PrerenderTiles {
    echo -e "That renders all tiles until Zoom level 12. This could take very long, depending on country size.\nIt should only be done once initially or when the style has changed."
    read -p "Do you want to continue? [y/N]" yn
    case $yn in
        [Yy]* ) ;;
        [Nn]* ) return;;
        * ) echo "The answer is something like no."; return;;
    esac
    GenerateMapStyle;
    #sudo docker exec --workdir=$DOCKERPATH_SOURCE_DIR --user=$DBPG_USERNAME $DOCKER_NAME /bin/bash -c "render_list -m $LAYER_NAME -a -z $MINZOOM -Z $MAXZOOM --num-threads=$RENDERING_THREADS"
    
    # to test rendering, take following command inside docker: sudo -u $DBPG_USERNAME render_list -m $LAYER_NAME_LOWERCASE -n $RENDERING_THREADS -z $MINZOOM -Z $MAXZOOM -a -s /run/renderd/renderd.sock -t /var/cache/renderd/tiles
    # to test rendering with coordinates, take following command inside docker: sudo -u $DBPG_USERNAME $DOCKERPATH_SOURCE_DIR/scripts/render_list_geo.pl -m $LAYER_NAME_LOWERCASE -l 50 -n $RENDERING_THREADS -z $MINZOOM -Z $MAXZOOM -x $MIN_LONG -X $MAX_LONG -y $MIN_LAT -Y $MAX_LAT -s /run/renderd/renderd.sock -t /var/cache/renderd/tiles
    Execute "sudo chmod ugo+x $DOCKERPATH_SOURCE_DIR/scripts/render_list_geo.pl"
    Execute "$DOCKERPATH_SOURCE_DIR/scripts/render_list_geo.pl -m $LAYER_NAME_LOWERCASE -l 50 -n $RENDERING_THREADS -z $MINZOOM -Z $MAXZOOM -x $MIN_LONG -X $MAX_LONG -y $MIN_LAT -Y $MAX_LAT -s /run/renderd/renderd.sock -t /var/cache/renderd/tiles" "" "$DBPG_USERNAME"
}
function GenerateTiles {
    GenerateMapStyle;
    echo Emptiing the style directory
    sudo rm -f -r $HOSTPATH_TILES_DIR/*
    # Tile generation is only possible with empty Tiles Directory           informations to OSMTILEMAKER: https://github.com/Magellium/osmtilemaker
    echo -e "${GREEN}Generate the tiles!${RESETCOLOR}"

    Execute "sudo -u $DBPG_USERNAME \
    python3 $DOCKERPATH_SOURCE_DIR/scripts/gen-tile.py \
    --bbox $BBOX \
    --bbox_name $BBOX_NAME \
    --mapfile $DOCKERPATH_WORKING_DIR/$LAYER_NAME/osm.xml \
    --tile_dir $DOCKERPATH_TILES_DIR \
    --minZoom $MINZOOM \
    --maxZoom $MAXZOOM \
    --num_threads $RENDERING_THREADS"
}
function DeleteDocker {
    read -p "This will delete the whole docker container. Are you sure? [y/n]" yn
    case $yn in
        [Yy]* ) ;;
        [Nn]* ) return;;
        * ) echo "Please answer yes or no."; return;;
    esac
    StopDocker;
    sudo docker compose rm || true
}
function StopDocker { 
    #sudo docker stop $DOCKER_NAME || true  # use "|| true" to ignore the error if it doesn't exist
    sudo docker compose stop || true  # use "|| true" to ignore the error if it doesn't exist
}
function CheckFiles {
    if [ ! -d $HOST_VOLUMES_BASE_DIR ]; then mkdir $HOST_VOLUMES_BASE_DIR; fi # create the directories if they doesn't exist
    if [ ! -d $HOSTPATH_WORKING_DIR ]; then mkdir $HOSTPATH_WORKING_DIR; fi
    if [ ! -d $HOSTPATH_OSM_FILE_DIR ]; then mkdir $HOSTPATH_OSM_FILE_DIR; fi
    if [ ! -d $HOSTPATH_PG_DATA_DIR ]; then mkdir $HOSTPATH_PG_DATA_DIR; fi
    if [ ! -d $HOSTPATH_OSM2PGSQL_FLATNODE_DIR ]; then mkdir $HOSTPATH_OSM2PGSQL_FLATNODE_DIR; fi
    if [ ! -d $HOSTPATH_TILES_DIR ]; then mkdir $HOSTPATH_TILES_DIR; fi
        
    MISSING_DIR=""
    if [ ! -f "$HOSTPATH_OSM_FILE_DIR/$OSM_LATEST_FILE_NAME" ]; then
        MISSING_DIR="$MISSING_DIR ${RED}$HOSTPATH_OSM_FILE_DIR/$OSM_LATEST_FILE_NAME does not exist.${RESETCOLOR} Please download it first using: wget -P ${HOSTPATH_OSM_FILE_DIR}/ ${OSM_LATEST_FILE_DOWNLOAD_URL}"
    fi
    #if [ ! -f "$HOSTPATH_OSM_FILE_DIR/raw.tif" ]; then  # not necessary any more as update_isolations was modified that it's not necessary
    #    MISSING_DIR="$MISSING_DIR ${RED}$HOSTPATH_OSM_FILE_DIR/raw.tif does not exist.${RESETCOLOR} It could be generated as described here: https://github.com/der-stefan/OpenTopoMap/tree/master/mapnik#hillshade-and-countours"
    #fi
    if [ ! -z "$MISSING_DIR" ]; then
        echo -e "Following files are missing:\n$MISSING_DIR"
        exit 1
    fi
}
function ReBuildDocker {
    DeleteDocker;
    CheckFiles;

    if [ ! -d $HOSTPATH_WORKING_DIR/$LAYER_NAME ]
    then
        # mkdir -p $HOSTPATH_WORKING_DIR
        echo 'Check out the layer from git'
        git clone https://gitlab.com/Apfelkuacha/$LAYER_NAME $HOSTPATH_WORKING_DIR/$LAYER_NAME
    fi
    
# Docker build replaced with docker compose
#    sudo docker build \
#    --tag $DOCKER_BUILD_TAG \
#    --build-arg INSTALLWORKDIR=$DOCKER_BUILD_ARG_INSTALLWORKDIR \
#    -f container/databaseDockerfile ./container
    sudo docker compose up -d
    #StartDocker;
    
	if [ "$(sudo ls -A $HOSTPATH_PG_DATA_DIR)" ]; then
        read -p "The Postgre Data directory is not empty, should it be deleted? [y/n]" yn
        case $yn in
            [Yy]* ) sudo rm -f -r $HOSTPATH_PG_DATA_DIR
                    echo "Directory removed, restart docker to make changes take effect"
                    StopDocker;
                    StartDocker;    # restart docker to recreate postgre settings
                    SetupPostgre;
                    Execute "cc -Wall -o $DOCKERPATH_WORKING_DIR/$LAYER_NAME/scripts/peak_isolation/isolation $DOCKERPATH_WORKING_DIR/$LAYER_NAME/scripts/peak_isolation/isolation.c -lgdal -lm -O2";
                    ImportDatabase;
                    GenerateMapStyle;
                    InitialiseDocker;;
            [Nn]* ) ;;
            * ) echo "Please answer yes or no."; return;;
        esac
	else
        SetupPostgre;
        GenerateMapStyle;
	fi
}

function SetupPostgre {
    Execute "sudo chmod ugo+x $DOCKERPATH_SOURCE_DIR/conf/postgres-settings.sh" # make the file executable
    Execute "$DOCKERPATH_SOURCE_DIR/conf/postgres-settings.sh" "ExecuteAsFile"
    StopDocker; # restart that Settings take effekt
    StartDocker;
    # to check the postgres settings: sudo docker exec $DOCKER_NAME bash -c 'cat ${PGDATA}/postgresql.conf'

    echo 'Create postgres user and database'
    Execute "sudo chmod ugo+x $DOCKERPATH_SOURCE_DIR/scripts/init-db.sh" # make the file executable
    #sudo docker exec --workdir=$DOCKERPATH_SOURCE_DIR --user=postgres $DOCKER_NAME "$DOCKERPATH_SOURCE_DIR/scripts/init-db.sh"
    Execute "$DOCKERPATH_SOURCE_DIR/scripts/init-db.sh" "ExecuteAsFile" "postgres"
}
function ConfigureDatabaseUpdate {
# example to delete a DB Table: sudo -u _renderd psql -d bikehikelayer -c "DROP TABLE public.planet_osm_replication_status;"
    
    Execute "osm2pgsql-replication init -d $DBPG_DATABASE_NAME --osm-file $DOCKERPATH_OSM_FILE_DIR/$OSM_LATEST_FILE_NAME" "" "$DBPG_USERNAME" # --server $OSM_UPDATE_FILE_DOWNLOAD_URL"   # Server not necessary if osm-file is used
#if you want to run it in docker as other user:    sudo -u $DBPG_USERNAME osm2pgsql-replication init -d $DBPG_DATABASE_NAME --osm-file $DOCKERPATH_OSM_FILE_DIR/$OSM_LATEST_FILE_NAME
    
#    echo ${GREEN}If you want to have daily updates early in the morning:${RESETCOLOR} run 'crontab -e' and insert '25 04 * * * sudo -u bikehikelayer /home/lukas/HikeBikeLayerServer/scripts/update-db_insideDocker.sh'
    echo "Configuring Database Update finished"
}
function RunDatabaseUpdate {
    #sudo docker exec --workdir=$DOCKERPATH_SOURCE_DIR --user=$DBPG_USERNAME $DOCKER_NAME /bin/bash -c "osm2pgsql-replication update -d $DBPG_DATABASE_NAME --post-processing $DOCKERPATH_SOURCE_DIR/scripts/expire_tiles.sh --max-diff-size 50  --  $OSM2PGSQL_OPTS --expire-tiles=8-16 --expire-output=/var/cache/renderd/dirty_tiles.txt"
    
    Execute "sudo chmod ugo+x $DOCKERPATH_SOURCE_DIR/scripts/update-db.sh" # make the file executable
    Execute "$DOCKERPATH_SOURCE_DIR/scripts/update-db.sh" "ExecuteAsFile" "$DBPG_USERNAME"
    #RestartRenderd;
}
function InitialiseDocker {
    Execute "sudo chmod ugo+x $DOCKERPATH_SOURCE_DIR/scripts/init-docker.sh" # make the file executable
    Execute "$DOCKERPATH_SOURCE_DIR/scripts/init-docker.sh" "ExecuteAsFile"

    #sudo docker exec --workdir=$DOCKERPATH_SOURCE_DIR $DOCKER_NAME /bin/bash -c "service renderd start" # starts it as user _renderd   -> but we want to run it as another user. Changing like here doesn't work: https://gis.stackexchange.com/q/414854
}

if ! [ -x "$(command -v docker)" ];then  echo 'Error: docker is not installed. Install it first, and do not run this script inside the container!' >&2;  exit 1; fi
if ! [ -x "$(command -v git)" ];then  echo 'Error: git is not installed.' >&2;  exit 1; fi

while true; do
    read -p "What do you want to do?    As a beginning check the config file, also the postgres-settings.sh file and then use: 9.  For serving using MOD_TILE and Webserver: additionaly use DC, MR
     1) Get into Docker commandline
     2) Get into Docker commandline using user root
     3) Edit config file
     4) Edit webserver index.html
     
     MS) Renew the map Style
     MG) Generate the tiles with OSMtileMaker
     MR) Prerender tiles
     
     DI) Import Database
     DC) Configure the automatic database Update
     DU) Update the map data (if no automatic database update is configured)
     
     ID) Load all values and services on docker
     
     6) Stop docker
     7) Start docker
     8) Delete the docker
     9) Restart from scratch, build up a new docker
     
     0) Exit
    " answer
    ReadConfig;
    case $answer in
        [1]* ) OpenDockerBash;;
        [2]* ) OpenDockerBashRoot;;
        [3]* ) nano .env;;
        [4]* ) Execute "sudo nano /var/www/html/index.html";;
        [Mm][Ss]* ) GenerateMapStyle;;
        [Mm][Gg]* ) GenerateTiles;;
        [Mm][Rr]* ) PrerenderTiles;;
        [Dd][Ii]* ) ImportDatabase;;
        [Dd][Cc]* ) ConfigureDatabaseUpdate;;
        [Dd][Uu]* ) RunDatabaseUpdate;;
        [Ii][Dd]* ) InitialiseDocker;;
        [6]* ) StopDocker;;
        [7]* ) StartDocker;;
        [8]* ) DeleteDocker;;
        [9]* ) ReBuildDocker;;
        [0]* ) exit;;
        [EeQq]* ) exit;;
        * ) echo "Please answer with a number or valid command.";;
    esac
done

