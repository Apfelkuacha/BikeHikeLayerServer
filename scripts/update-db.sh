#!/bin/bash
echo
echo "update_tiles.sh run from directory $PWD with process ID $$ on:" `date`
. /docker_mounted_volumes/sources/.env    # read configuration

#
if ! command -v osm2pgsql-replication &> /dev/null
then
    echo "osm2pgsql-replication could not be found"
    exit 1
fi
#
if [[ -f /var/cache/renderd/update_tiles.sh.running ]]
then
    echo "update_tiles.sh already running; /var/cache/renderd/update_tiles.sh.running exists"
    exit 1
else
    touch /var/cache/renderd/update_tiles.sh.running
fi
# /usr/local/bin/    Directory found with: whereis osm2pgsql-replication
#if ! osm2pgsql-replication update -d $DBPG_DATABASE_NAME --post-processing $DOCKERPATH_SOURCE_DIR/scripts/expire_tiles.sh --max-diff-size 20  -- $OSM2PGSQL_OPTS --expire-tiles=8-16 --expire-output=/var/cache/renderd/dirty_tiles.txt   # to have the output directly for debugging
if ! osm2pgsql-replication update -d $DBPG_DATABASE_NAME --post-processing $DOCKERPATH_SOURCE_DIR/scripts/expire_tiles.sh --max-diff-size 20  -- $OSM2PGSQL_OPTS --expire-tiles=8-16 --expire-output=/var/cache/renderd/dirty_tiles.txt > /var/cache/renderd/osm2pgsql-replication.$$ 2>&1
then
    echo "osm2pgsql-replication error"
    cat /var/cache/renderd/osm2pgsql-replication.$$
else
    grep Backlog /var/cache/renderd/osm2pgsql-replication.$$ | tail -1
    rm /var/cache/renderd/osm2pgsql-replication.$$  # delete the logfile if it was successful
fi
#
rm /var/cache/renderd/update_tiles.sh.running
#
