#!/bin/bash

if [[ ! -f $DOCKERPATH_WORKING_DIR/$LAYER_NAME/osm.xml ]]
then
    echo "The style of the map hasn't been generated, do that first with MS that the webservice and renderd could work"
    exit 0
fi

if [[ -f /var/cache/$LAYER_NAME.initFinished ]]
then
    echo "The running docker image was already configured, only start services now"
    chown $DBPG_USERNAME:$DBPG_USERNAME $DOCKERPATH_TILES_DIR
    sudo chmod ugo+x $DOCKERPATH_SOURCE_DIR/scripts/expire_tiles.sh
    sudo chmod -R ugo+wx /var/cache/renderd/
    cron    # start cron to check for the daily map updates
    sudo chmod ugo+x $DOCKERPATH_SOURCE_DIR/scripts/update-db.sh # make the file executable
    chown -R $DBPG_USERNAME:$DBPG_USERNAME /var/lib/mod_tile
    chown -R $DBPG_USERNAME:$DBPG_USERNAME /var/cache/renderd
    chown -R $DBPG_USERNAME:$DBPG_USERNAME /run/renderd
    if ps aux | grep [r]enderd | grep -v grep > /dev/null; then echo "renderd is already running"; else sudo -u $DBPG_USERNAME renderd; fi
    #sudo -u $DBPG_USERNAME service renderd start # hint: "service renderd status" tells that it failed. But with "sudo -u _renderd service renderd status" you could see that it's running
    service apache2 start
    echo "starting services finished"
    exit 0
else
    echo "Start configuration of the docker image"
    ln -s $DOCKERPATH_TILES_DIR /var/www/html/local-tiles || true
    grep -qF $DOMAIN_NAME /etc/apache2/apache2.conf || echo -e "ServerName $DOMAIN_NAME" >> /etc/apache2/apache2.conf   # insert only if it doesn't exist yet
    grep -qF "[$LAYER_NAME_LOWERCASE]" /etc/renderd.conf || echo -e "\n; MY OWN LAYERS:\n[$LAYER_NAME_LOWERCASE]\nURI=/$LAYER_NAME/\nXML=$DOCKERPATH_WORKING_DIR/$LAYER_NAME/osm.xml\nHOST=$DOMAIN_NAME\nMAXZOOM=17" >> /etc/renderd.conf   # the map name has to be lowercase, otherwise renderd tells "no map for BikeHikeLayer"
    useradd $DBPG_USERNAME || true
    chown $DBPG_USERNAME:$DBPG_USERNAME $DOCKERPATH_TILES_DIR

    # Database -> it is already configured!
    #sudo chmod ugo+x $DOCKERPATH_SOURCE_DIR/scripts/init-db.sh
    #sudo -u postgres $DOCKERPATH_SOURCE_DIR/scripts/init-db.sh

    # Configure Database update
    sudo chmod ugo+x $DOCKERPATH_SOURCE_DIR/scripts/expire_tiles.sh
    sudo chmod -R ugo+wx /var/cache/renderd/
    mkdir -p /var/lib/mod_tile
    rm -f /var/cache/renderd/update_tiles.sh.running
    cron    # start cron to check for the daily map updates
    mkdir -p /var/cache/renderd/osm2pgsql-replicationCronjob
    chown -R $DBPG_USERNAME:$DBPG_USERNAME /var/cache/renderd/osm2pgsql-replicationCronjob
    sudo chmod ugo+x $DOCKERPATH_SOURCE_DIR/scripts/update-db.sh # make the file executable
    sudo echo -e "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:\n40 00 * * * $DOCKERPATH_SOURCE_DIR/scripts/update-db.sh >> /var/cache/renderd/osm2pgsql-replicationCronjob/UpdateLog\$(date +\%Y\%m\%d).log 2>&1\n" > /etc/cron.d/updateDatabase
    chmod 644 /etc/cron.d/updateDatabase
    crontab -u $DBPG_USERNAME /etc/cron.d/updateDatabase
    echo "Configuring Database Update finished"

    # Webserver
    cp $DOCKERPATH_SOURCE_DIR/website/* /var/www/html/
    sed -i -e"s/^num_threads=4.*$/num_threads=10/" /etc/renderd.conf
    mkdir -p /run/renderd
    mkdir -p /var/lib/mod_tile
    chown -R $DBPG_USERNAME:$DBPG_USERNAME /var/lib/mod_tile
    chown -R $DBPG_USERNAME:$DBPG_USERNAME /var/cache/renderd
    chown -R $DBPG_USERNAME:$DBPG_USERNAME /run/renderd
    pkill renderd || true
    sudo -u $DBPG_USERNAME renderd    # start renderd not with "service renderd start" as it doesn't work for unknown reason...
    # start renderd as other user as "service renderd start" doesn't work, reason unknown. Method to change the user for the service: change service user: https://gis.stackexchange.com/questions/414854/osm-tile-server-renderd-keeps-using-wrong-user
    #sed -i "/RUNASUSER=/c\RUNASUSER=$DBPG_USERNAME" /etc/init.d/renderd
    #sudo -u $DBPG_USERNAME service renderd restart # hint: "service renderd status" tells that it failed. But with "sudo -u _renderd service renderd status" you could see that it's running
    service apache2 restart
    echo "Configuration finished"
    touch /var/cache/$LAYER_NAME.initFinished
    exit 0
fi
