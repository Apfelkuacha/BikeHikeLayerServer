#!/bin/bash
# this deletes tiles starting from zoom level 16, mark tiles from 15 as old and re-renders all other tiles. 
# Rendering tiles from zoom 9 to 12 takes much longer as loads of data from postgre is read because it's a large area of the map.
echo call render_expired to mark the changed tiles as invalid
render_expired --map=bikehikelayer --min-zoom=9 --max-zoom=16 --touch-from=15 --delete-from=18 -s /run/renderd/renderd.sock -t /var/cache/renderd/tiles < /var/cache/renderd/dirty_tiles.txt
rm /var/cache/renderd/dirty_tiles.txt
echo render_expired finished
